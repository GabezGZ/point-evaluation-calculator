package com.gabez.pointevaluationcalculator

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.ArrayList

class EvAdapter(private val mItem: ArrayList<Item>, private val mRecyclerView: RecyclerView) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class EvHolder(pItem: View) : RecyclerView.ViewHolder(pItem) {
        var mMain: TextView
        var mBase: TextView
        var mContainer: ViewGroup

        init {
            mMain = pItem.findViewById(R.id.evMain)
            mBase = pItem.findViewById(R.id.evBase)
            mContainer=pItem.findViewById(R.id.evContainer)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {

        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_ev, viewGroup, false)

        view.setOnLongClickListener(object: View.OnLongClickListener{
            override fun onLongClick(v: View?): Boolean {
                deleteEv(v)
                return false
            }
        })

        return EvHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = mItem[position]
        (viewHolder as EvHolder).mMain.setText(item.main.toInt().toString())
        viewHolder.mBase.setText(item.base.toInt().toString())

        if(viewHolder.mBase.text.equals("0")){
            viewHolder.mContainer.setBackgroundResource(R.drawable.bg_box_shadow_2)
        }
    }


    override fun getItemCount(): Int {
        return mItem.size
    }

    fun deleteEv(v: View?){
        val position: Int = mRecyclerView.getChildAdapterPosition(v)
        mItem.removeAt(position)
        notifyItemRemoved(position)
    }

    companion object {

    }


}