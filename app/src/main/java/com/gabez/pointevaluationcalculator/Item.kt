package com.gabez.pointevaluationcalculator

class Item {

    var main: Float= 1.0F
    var base: Float=1.0F

    constructor(newMain: Float, newBase: Float){
        main=newMain
        base=newBase
    }

    constructor(){
        main=1.0F
        base=1.0F
    }
}