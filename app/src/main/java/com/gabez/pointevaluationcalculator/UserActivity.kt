package com.gabez.pointevaluationcalculator

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AlertDialog
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class UserActivity : AppCompatActivity(), FragmentSubject.OnFragmentInteractionListener {

    lateinit var evView: RecyclerView
    lateinit var evAdd: FloatingActionButton
    lateinit var tvSrednia: TextView
    lateinit var tvDoLepszej: TextView
    var testList: ArrayList<Item> = ArrayList()
    var srednia: Float=100.0F
    var main: Float= 0F
    var base: Float=0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        tvSrednia=findViewById(R.id.subjectTextViewSrednia)
        tvDoLepszej=findViewById(R.id.subjectTextViewGoal)

        evView=findViewById(R.id.subjectRecyclerView)
        evView.adapter=EvAdapter(testList, evView)
        evView.setHasFixedSize(true)
        evView.layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)
        evView.itemAnimator = DefaultItemAnimator()

        evAdd=findViewById(R.id.floatingActionButton)
        evAdd.setOnClickListener(View.OnClickListener {
            showPopup()
            Toast.makeText(applicationContext, "Click", Toast.LENGTH_SHORT)
        })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.evaluations_menu, menu)
        return true
    }

    override fun onFragmentInteraction(uri: Uri){

    }

    fun addEv(main: Int, base: Int){
        testList.add(Item(main.toFloat(), base.toFloat()))
        evView.adapter.notifyDataSetChanged()
        countStuff()
        countToBeBetter()
    }

    fun countStuff(){
        if(testList.isNotEmpty()){

            for(item in testList){
                main+=item.main
                base+=item.base
            }

            if(!base.equals(0F)){
                srednia=main/base*100
                tvSrednia.text=("Średnia z przedmiotu: "+srednia.toInt()+"%")
            }else{
                tvSrednia.text=("Średnia z przedmiotu: "+(main*100).toInt()+"%")
            }
        }
    }

    fun countToBeBetter(){
        var pointsToBeBetter: Float=0F
        var newMain: Float=main
        var newBase: Float=base
        var pomocniczaSrednia: Float=0F
        var wyzsza: String=""

        if(!newBase.equals(0F)){
            pomocniczaSrednia=newMain/newBase
        }

        if(srednia>=100F){

        }else if(srednia>=90F&&srednia<100F){
            do{
                newMain++
                newBase++
                pomocniczaSrednia=newMain/newBase
            }while(pomocniczaSrednia>=100F)
            wyzsza="(6)"

        }else if(srednia>=85F&&srednia<90F){
            do{
                newMain++
                newBase++
                pomocniczaSrednia=newMain/newBase
            }while(pomocniczaSrednia>=90F)
            wyzsza="(5)"

        }else if(srednia>=70F&&srednia<85F){
            do{
                newMain++
                newBase++
                pomocniczaSrednia=newMain/newBase
            }while(pomocniczaSrednia>=85F)
            wyzsza="(4+)"

        }else if(srednia>=65F&&srednia<70F){
            do{
                newMain++
                newBase++
                pomocniczaSrednia=newMain/newBase
            }while(pomocniczaSrednia>=70F)
            wyzsza="(4)"

        }else if(srednia>=50F&&srednia<65F){
            do{
                newMain++
                newBase++
                pomocniczaSrednia=newMain/newBase
            }while(pomocniczaSrednia>=65F)
            wyzsza="(3+)"

        }else if(srednia>=40F&&srednia<50F){
            do{
                newMain++
                newBase++
                pomocniczaSrednia=newMain/newBase
            }while(pomocniczaSrednia>=50F)
            wyzsza="(3)"

        }else{
            do{
                newMain++
                newBase++
                pomocniczaSrednia=newMain/newBase
            }while(pomocniczaSrednia>=40F)
            wyzsza="(2)"
        }
        pointsToBeBetter=newMain-main
        tvDoLepszej.text=("Do wyższej "+wyzsza+" oceny brakuje: "+pointsToBeBetter.toInt()+"pkt")
    }

    fun showPopup(){
        var mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var mView: View = layoutInflater.inflate(R.layout.popup_add, null)
        var main: EditText = mView.findViewById(R.id.etMain)
        var base: EditText= mView.findViewById(R.id.etBase)
        var add: Button = mView.findViewById(R.id.btAdd)

        mBuilder.setView(mView)
        var dialog: AlertDialog= mBuilder.create()
        dialog.show()

        add.setOnClickListener(View.OnClickListener{
                if(!main.getText().toString().isEmpty() && !base.getText().toString().isEmpty()){
                    addEv((Integer.parseInt(main.text.toString())), (Integer.parseInt(base.text.toString())))
                }
                dialog.dismiss()
        })
    }

}


